import { GET_PRODUCT_LIST } from "../../Type/Types";
import axios from "axios";

const getproductslistapi = (data) => ({
  type: GET_PRODUCT_LIST,
  payload: data,
});

export const getProductlist = () => {
  return (dispatch) => {
    axios
      .get('http://localhost:8000/products/')
      
      .then((data) => {
        // console.log(data);
        dispatch(getproductslistapi(data));
      })
      .catch((e) => {});
  };
};
