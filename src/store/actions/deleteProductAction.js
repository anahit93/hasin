import { DELETE_SELECTED_PRODUCT } from "../../Type/Types";


const deleteSelectedProduct = (index) => ({
  type: DELETE_SELECTED_PRODUCT,
  payload: index,
});

export const deleteSelected = (productid) => {
  return (dispatch) => {
        dispatch(deleteSelectedProduct(productid));
    
  };
};