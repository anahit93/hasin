import { DELETE_SELECTED_PRODUCT } from "../../Type/Types";

const INITIAL_STATE = {
  data: [],
};

export default function deleteSelected(state = [INITIAL_STATE], action) {
  switch (action.type) {
    case DELETE_SELECTED_PRODUCT:
      return {
        data: [...state.data.filter((index) => index !== action.payload)],
      };
    default:
      return state;
  }
}
