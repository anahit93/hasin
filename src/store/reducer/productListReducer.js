import {GET_PRODUCT_LIST} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}


export default function getProductlist( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_PRODUCT_LIST:
            return {
                data:action.payload.data
    };
   
        default:
            return state;
    }
    
    
}
