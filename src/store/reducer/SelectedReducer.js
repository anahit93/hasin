import {GET_SELECTED_ASSET} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getSelected( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_SELECTED_ASSET:
            return {
                data:action.payload
    };
   
        default:
            return state;
    }
    
    
}
