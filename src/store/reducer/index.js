import { combineReducers } from 'redux';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import getProductlist from './productListReducer';
import getSelected from './SelectedReducer';
import deleteSelected from './deleteSelectedReducer';


export default combineReducers({
    getProductlist :persistReducer({key: 'getKeyProductlist',storage,whiteList:['data']},getProductlist),
    getSelected :persistReducer({key: 'getKeySelected',storage,whiteList:['data']},getSelected),
    deleteSelected :persistReducer({key: 'deleteKeySelected',storage,whiteList:['data']},deleteSelected),
})