import React from 'react';
import { Provider } from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor,store} from './store';
import Navbar from './component/Navbar'
import './App.css';



class App extends React.Component{

    render(){
        return(
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                            <Navbar/>         
                </PersistGate>
            </Provider>
        )
    }
}


export default App;
