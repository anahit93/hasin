import React, { Component } from "react";

class Button extends Component {
  state = {};
  render() {
    return (
      <div className={this.props.btnclass} onClick={this.props.onClick}>
        {this.props.text}
      </div>
    );
  }
}

export default Button;
