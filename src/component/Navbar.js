import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import Home from "./Home";
import NotFound from "./NotFound";
import AboutUs from "./AboutUs";
import ProductDetail from "./ProductDetail";

class Navbar extends Component {
  state = {};
  render() {
    return (
      <div>
        <Router>
          <div className="d-flex flex-column">
            <div>
              <header className="header">
                <label className="menu-icon" htmlFor="menu-btn">
                  <span className="navicon"></span>
                </label>
                <input className="menu-btn" type="checkbox" id="menu-btn" />
                <ul className="menu">
                  <li>
                    <NavLink
                      to="/AboutUs"
                      className="main-nav"
                      activeClassName="main-nav-active"
                    >
                      درباره ما
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      exact
                      to="/"
                      className="main-nav"
                      activeClassName="main-nav-active"
                    >
                      خانه
                    </NavLink>
                  </li>
                </ul>
              </header>
            </div>
            <div className="pt-5 switchclass">
              <Switch>
                <Route exact path="/ProductDetail/:id">
                  <ProductDetail />
                </Route>

                <Route path="/AboutUs">
                  <AboutUs />
                </Route>

                <Route exact path="/">
                  <Home />
                </Route>

                <Route path="*">
                  <NotFound />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default Navbar;
