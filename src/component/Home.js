import React, { Component } from "react";
import { connect } from "react-redux";
import { getProductlist } from "../store/actions/getProductListAction";
import ProductList from "./ProductList";

class Home extends Component {
  state = {};

  componentDidMount() {
    this.props.getProductlist();
    console.log(this.props.products);
  }

  render() {
    return (
      <div className="Homecomponent">
        <ProductList />
      </div>
    );
  }
}

const mapStateToProps = ({ getProductlist }) => {
  return {
    products: getProductlist.data,
  };
};

export default connect(mapStateToProps, {
  getProductlist,
})(Home);
