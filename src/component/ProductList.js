import React, { Component } from "react";
import { connect } from "react-redux";
import { getProductlist } from "../store/actions/getProductListAction";
import { deleteSelected } from "../store/actions/deleteProductAction";
import Button from "./‌Button";
import { getSelected } from "../store/actions/getSelectedAction";
import { BrowserRouter as Router, Link } from "react-router-dom";

class ProductList extends Component {
  state = { products2: [] };

  componentDidMount() {
    this.setState({ products2: this.props.products });
  }

  onDelete=(id)=>{
    console.log(id)
    const copy=[...this.state.products2];
   const test=copy.filter(product=>product.id!==id)
    this.setState({products2:test})
  }

  render() {
    console.log(this.props.products);
    console.log(this.state.products2);
    return (
      <div className="container">
        <div className="row productrow">
          {this.state.products2.map((product) => {
            return (
              <div className="col-lg-4 col-6 text-center p" key={product.id}>
                <Link to={`/ProductDetail/${product.id}`}>
                  <div
                    className="productbox"
                    onClick={() => this.props.getSelected(product)}
                  >
                    <img
                      src={`products/${product.sku}_2.jpg`}
                      alt={product.ttile}
                      className="mt-2"
                    />
                    {product.title}
                  </div>
                </Link>
                <Button
                  btnclass="btn btn-danger m-3 deletebtn"
                  text="delete"
                  onClick={()=>this.onDelete(product.id)}
                  // onClick={()=>this.props.deleteSelected(product.id)}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ getProductlist, getSelected,deleteSelected }) => {
  return {
    products: getProductlist.data,
    selected: getSelected.data,
    productsAfterDelete:deleteSelected.data
  };
};

export default connect(mapStateToProps, {
  getProductlist,
  getSelected,
  deleteSelected
})(ProductList);
