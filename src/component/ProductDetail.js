import React, { Component } from "react";
import { getSelected } from "../store/actions/getSelectedAction";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import Button from "./‌Button";

class ProductDetail extends Component {
  state = { title: "", description: "", sku: "", price: "" };

  componentDidMount() {
    this.props.getSelected();
    console.log(this.props.selected);
    this.setState({ title: this.props.selected.title });
    this.setState({ description: this.props.selected.description });
    this.setState({ sku: this.props.selected.sku });
    this.setState({ price: this.props.selected.price });
  }

  render() {
    console.log(this.state.size);
    return (
      <div className="ProductDetailComponnet">
        <img src={`../products/${this.state.sku}_2.jpg`} />
        <div>
          <strong>Title: </strong>
          {this.state.title}
        </div>
        <div>
          <strong>Description: </strong>
          {this.state.description}
        </div>
        <div>
          <strong>Price: </strong>
          {this.state.price}
        </div>

        <div className="d-flex justify-content-center mt-3">
          <Button
            btnclass="btn btn-success backbtn"
            text="back"
            onClick={this.props.history.goBack}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ getSelected }) => {
  return {
    selected: getSelected.data,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {
    getSelected,
  })
)(ProductDetail);
