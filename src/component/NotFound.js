import React from "react";
import error404 from "../assets/img/404.svg";

const NotFound = () => (
  <div className="notfound">
    <img src={error404} className="erroricon" />

    <h3>Page not found</h3>
  </div>
);

export default NotFound;
