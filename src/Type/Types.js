export const GET_PRODUCT_LIST  = "get_product_list"

export const GET_SELECTED_ASSET = "get_selected_asset"

export const DELETE_SELECTED_PRODUCT = "delete_selected_product"
