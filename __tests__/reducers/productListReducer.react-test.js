import request from '../../src/store/reducer/productListReducer'
import {GET_PRODUCT_LIST} from '../../src/Type/Types';

describe('Reuest Reducer',()=>{
    it('has a default state',()=>{

        expect(request(undefind,{type:'unexpected'})).toEqual({
            data:[]  
        })
    })


    it('can handle GET_PRODUCT_LIST',()=>{
        expect(request(undefind,{type:GET_PRODUCT_LIST,
        payload:{
            data:[]
        }
        })).toEqual({
            data:[]  
        })
    })


})