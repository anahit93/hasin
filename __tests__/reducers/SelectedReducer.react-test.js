import request from '../../src/store/reducer/SelectedReducer'
import {GET_SELECTED_ASSET} from '../../src/Type/Types';

describe('Reuest Reducer',()=>{
    it('has a default state',()=>{

        expect(request(undefind,{type:'unexpected'})).toEqual({
            data:'' 
        })
    })


    it('can handle GET_SELECTED_ASSET',()=>{
        expect(request(undefind,{type:GET_SELECTED_ASSET,
        payload:{
            data:''
        }
        })).toEqual({
            data:'' 
        })
    })


})